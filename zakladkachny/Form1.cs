﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Diagnostics;

namespace zakladkachny
{
    public partial class Form1 : Form
    {
        int skore = 0;

        Button prvni;
        Button druhy;

        int pocetTicku = 0;

        int startTick = 0;

        Random rnd = new Random();
        
        Stopwatch stopWatch = new Stopwatch();

        long time = 0;

        public Form1()
        {
            InitializeComponent();
            prvni = new Button();
            prvni.BackgroundImage = Image.FromFile("kachna.jpg");
            prvni.BackgroundImageLayout = ImageLayout.Zoom;
            prvni.Left = 100;
            prvni.Top = 10;
            prvni.Size = new Size(100,100);
            prvni.Click += this.Clicked;
            this.Controls.Add(prvni);
            druhy = prvni;
            this.Controls.Add(druhy);
            timer.Enabled = true;
            label1.Text = "Skore: " + skore;
            stopWatch.Start();
        }

        public void Clicked(object sender, EventArgs args)
        {
            prvni.Location = new Point(rnd.Next(0,this.Width-prvni.Width), 0);
            skore++;
            label1.Text = "Skore: " + skore;
            startTick = pocetTicku;
        }

        private void timer_Tick(object sender, EventArgs e)
        {
            time = stopWatch.ElapsedMilliseconds;
            label2.Text = (/*(long)*/(time / 1000)).ToString();
            pocetTicku++;
            prvni.Location = new Point(prvni.Location.X+(rnd.Next(0,3)-1)*5,(int)(prvni.Location.Y+1+(pocetTicku - startTick)*0.1f));
            if(prvni.Location.Y > this.Height + 100)
            {
                prvni.Location = new Point(rnd.Next(0, this.Width - prvni.Width), 0);
                startTick = pocetTicku;
                skore--;
                label1.Text = "Skore: " + skore;
            }
            if(time > 50 * 1000)
            {
                label1.Text = "Konec hry se skorem: " + skore;
            }
        }
    }
}
//tohle jsem napsal ja c# HAHAHA
